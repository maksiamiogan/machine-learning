import numpy as np
import pandas as pd

class AdalineGD(object):
	"""docstring for AdalineGD
 	Parameters
    ------------
    eta : float
      Learning rate (between 0.0 and 1.0)
    n_iter : int
      Passes over the training dataset.
    random_state : int
      Random number generator seed for random weight
      initialization.

    Attributes
    -----------
    w_ : 1d-array
      Weights after fitting.
    errors_ : list
      Number of misclassifications (updates) in each epoch.
	"""	
	def __init__(self, eta = 0.01, n_iter = 50, random_state = 1):
		self.eta = eta
		self.n_iter = n_iter
		self.random_state = random_state
	def fit(self, X, y):
		"""Fit training data.

        Parameters
        ----------
        X : {array-like}, shape = [n_examples, n_features]
          Training vectors, where n_examples is the number of examples and
          n_features is the number of features.
        y : array-like, shape = [n_examples]
          Target values.
        
        Returns
        -------
        self : object
        """
        rgen = np.random.RandomState(self.random_state) # exposes a number of methods for generating random numbers drawn from a variety of probability distributions.
        self.w_ = rgen.normal(loc = 0.0, scale = 0.01, size = 1 + X.shape[1])
        self.cost_ = []

        for i in range(self.n_iter):
          net_input = self.net_input(X)
          output = self.activation(net_input)
          errors = y - output
    			self.w_[1:] += self.eta * X.T.dot(errors)
    			self.w_[0] += self.eta * errors.sum()
          cost = (errors**2).sum() / 2.0
    		  self.cost_.append(cost)
    	return self

    def net_input(self, X):
    	"""Calculate net input"""
    	return np.dot(X, self.w_[1:]) + self.w_[0]

    def activation(self, X):
      return X


    def predict(self, X):
    	"""Return class label after unit step"""
    	return np.where(self.activation(self.net_input(X)) >= 0.0, 1, -1) #those el which more then 0.0 will be replaced by 1 and another'll be replaced by -1

df = pd.read_csv("iris.data", header = None, encoding = 'utf-8')

y = df.iloc[0:100, 4].values
y = np.where(y == "Iris-setosa", -1, 1)

X = df.iloc[0:100, [0,2]].values

X_std = np.copy(X)
X_std[:,0] = (X[:,0] - X[:,0].mean() )/ X[:,0].std()
X_std[:,1] = (X[:,1] - X[:,1].mean() )/ X[:,1].std()


#Training the adaline model

ppn = AdalineGD(eta= 0.1, n_iter = 10)

ppn.fit(X_std,y)

		