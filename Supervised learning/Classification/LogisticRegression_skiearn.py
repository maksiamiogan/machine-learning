from sklearn.linear_model import LogisticRegression

lr = LogisticRegression(C = 100.0, random_state = 1, solver = "lbfgs", multi_class = 'ovr')
lr.fit(X_train_std, y_train)

#Веротяность того, что обучающие образцы принадлежат определенному классу
lr.predict_proba(X_test_std[:3,:])