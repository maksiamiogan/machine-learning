import numpy as np
import pandas as pd

class AdalineSGD(object):
	"""docstring for AdalineSGD
 	Parameters
    ------------
    eta : float
      Learning rate (between 0.0 and 1.0)
    n_iter : int
      Passes over the training dataset.
    random_state : int
      Random number generator seed for random weight
      initialization.

    Attributes
    -----------
    w_ : 1d-array
      Weights after fitting.
    errors_ : list
      Number of misclassifications (updates) in each epoch.
	"""	

	def __init__(self, eta = 0.01, n_iter = 50, random_state = None, shuffle = True):
		self.eta = eta
		self.n_iter = n_iter
		self.random_state = random_state
    self.shuffle = shuffle

	def fit(self, X, y):
		"""Fit training data.

        Parameters
        ----------
        X : {array-like}, shape = [n_examples, n_features]
          Training vectors, where n_examples is the number of examples and
          n_features is the number of features.
        y : array-like, shape = [n_examples]
          Target values.
        
        Returns
        -------
        self : object
        """
        self._initialize_weights(X.shape[1])
        self.cost_ = []
        for i in range(self.n_iter):
          if self.shuffle:
            X, y = self._shuffle(X,y)
          cost = []
          for xi, target in zip(X,y):
            cost.append(self._update_weights(xi,target))
          avg_cost = sum(cost)/len(y)
          self.cost_.append(avg_cost)
        return self
    def partial_fit(self,X,y):
      if not self.w_initialized:
        self._initialize_weights(X.shape[1])
      if y.ravel().shape[0] > 1:
        for xi, target in zip(X,y):
          self._update_weights(xi, target)
      else:
        self._update_weights(X,y)
      return self

    def _shuffle(self, X, y):
      r = self.rgen.permutation(len(y))
      return X[r], y[r]

    def _initialize_weights(self, m):
      self.rgen = np.random.RandomState(self.random_state)
      self.w_ = self.rgen.normal(loc = 0.0, scale = 0.01, size = 1 + m)
      self.w_initialized = True

    def _update_weights(self, xi, target):
      output = self.activation(self.net_input(xi))
      error = (target - output)
      self.w_[1:] += self.eta * xi.dot(error)
      self.w_[0] += self.eta * error
      cost = 0.5 * error**2
      return cost

    def net_input(self, X):
    	"""Calculate net input"""
    	return np.dot(X, self.w_[1:]) + self.w_[0]

    def activation(self, X):
      return X

    def predict(self, X):
      """Return class label after unit step"""
      return np.where(self.activation(self.net_input(X)) >= 0.0, 1, -1) #those el which more then 0.0 will be replaced by 1 and another'll be replaced by -1

df = pd.read_csv("iris.data", header = None, encoding = 'utf-8')

y = df.iloc[0:100, 4].values
y = np.where(y == "Iris-setosa", -1, 1)

X = df.iloc[0:100, [0,2]].values

X_std = np.copy(X)
X_std[:,0] = (X[:,0] - X[:,0].mean() )/ X[:,0].std()
X_std[:,1] = (X[:,1] - X[:,1].mean() )/ X[:,1].std()

#Training the perceptron model

ppn = AdalineSGD(eta= 0.1, n_iter = 10)

ppn.fit(X_std,y)

		